package me.Râe.Tutorial.Events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EventsClass implements Listener {

    public String prefix = (ChatColor.BLUE + "Tutorials >>");

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        player.sendMessage(prefix + ChatColor.GOLD + "Welcome back," + player.getName());
    }
}
